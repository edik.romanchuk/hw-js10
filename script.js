const tabMenuItems = document.querySelectorAll('.tabs-title');
const tabContent = document.querySelectorAll('.content');

for(let i = 0; i < tabMenuItems.length; i++){
    tabMenuItems[i].addEventListener('click', (e) => {
        
        let tabCurrent = e.target.parentElement.children;
        for(let j = 0; j < tabCurrent.length; j++){
            tabCurrent[j].classList.remove('active')
        }
        e.target.classList.add('active');

        let contentCurrent = e.target.parentElement.nextElementSibling.children;
        for(let c = 0; c < contentCurrent.length; c++){
            contentCurrent[c].classList.remove('content-active')
        }

        tabContent[i].classList.add('content-active');       
        
    });
}
